import numpy as np
from itertools import product as product
import matplotlib.pyplot as plt
import os

def matrix_strider(matrix_shape, win_shape, axis_strides=None):  
    """
    Simulates sliding of a window (multi-D) over a matrix (multi-D) 
    and generates slices to access the overlaps.
    
    Params:
    --------
    matrix_shape, tuple
        Shape of the matrix over which we will slide.
        
    win_shape
        Shape of the sliding window.

    axis_strides, optional, default None
        Allows for a different stride in each axis.
        If None, set to win_shape.
        
    Example:
    --------
    matrix_strider(matrix_shape=(5,4), win_shape=(3,2), axis_strides=(3,2))
    
    >>[(slice(0, 3, None), slice(0, 2, None)),
    >> (slice(0, 3, None), slice(2, 4, None)),
    >> (slice(3, 6, None), slice(0, 2, None)),
    >> (slice(3, 6, None), slice(2, 4, None))] 
    """
    
    def axis_strider(axis_len, win_len, stride):
        """
        Simulates sliding of a 1D window over one axis and generates slices in that direction.
        """
        for i in range(0, axis_len, stride):
            yield slice(i, i+ win_len)


    assert len(matrix_shape) == len(win_shape), 'Sliding window must have same ndim as matrix.'
    if axis_strides is None:
        axis_strides = win_shape
    axis_striders = [axis_strider(axis_len, win_shape[i], axis_strides[i]) 
                     for i, axis_len in enumerate(matrix_shape)]

    yield from product(*axis_striders)
    
    
def occlude(matrix, slicer):
    """Returns a matrix zeroed at specified slicer."""
    occluded = matrix.copy()
    occluded[slicer] = 0
    return occluded
    
    
def sliding_occluder(matrix, win_shape, axis_strides=None):
    """ 
    Slides through a matrix with a window of specified shape and
    yields matrix occluded with zeros at the window position.
    """
    for slicer in matrix_strider(matrix.shape, win_shape, axis_strides):
        yield occlude(matrix, slicer), slicer


def get_occlusion_map(model, image, win_shape=(1,3,3), axis_strides=None):
    """
    Get the occlusion map for one image on a particular model checkpoint.
    Value in the occlusion map shows, how much the prediction changed with that
    area occluded. Positive values mean, the prediction got worse, i.e. that
    the area is important, because without it, the performance drops. 
    Negative value shows, that the occluded area is confusing the prediction, 
    because without it, the prediction is better. These areas are important
    for classification into a different class.
    
    
    Returns
    --------
        Occlusion map normalized from 0 to 1
        Predicted class of the image 
        Predicted probability of the image belonging to the predicted class
        Predicted probability of the occluded image to belong in the predicted class
    """
    assert image.ndim == 3, 'Provide a single image of ndim == 3, without batch axis.'
    
    heat_map = np.ones_like(image).astype(np.float32) * -1
    y_hat = model.predict(np.array([image]))[0]  # Original prediction probability
    class_idx = np.argmax(y_hat)  # Original prediction class

    for ocluded, slicer in sliding_occluder(image, win_shape=win_shape, axis_strides=axis_strides):
        pred = model.predict(np.array([ocluded]))[0]
        heat_map[slicer] = y_hat[class_idx] - pred[class_idx]
    
    return heat_map, class_idx, y_hat


def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Credit to:
    https://stackoverflow.com/questions/7404116/defining-the-midpoint-of-a-colormap-in-matplotlib
    
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero.

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower offset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to 
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax / (vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highest point in the colormap's range.
          Defaults to 1.0 (no upper offset). Should be between
          `midpoint` and 1.0.
    '''
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False), 
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))
    
    from matplotlib.colors import LinearSegmentedColormap
    newcmap = LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap


def plot_occ(x, y, occ, size=(5,5), identifier='', suptitle=False, save_dir=None, ftype='.pdf', show=True, posit_occ=True, win_shape='', dpi=100, quality=95):

    from matplotlib.cm import bwr_r
    
    occ_map, class_idx, y_hat = occ
    multiplier_occ_map = occ_map.copy()
    if posit_occ:
        multiplier_occ_map[occ_map<0] = 0 # Only positive occ map
    imgs = [x, occ_map, x*multiplier_occ_map]
    
    plt.figure(figsize=(size[1]*len(imgs), x.shape[0]*size[0]))
    plt.rcParams.update({'font.size': 12})
    plt.rcParams["font.family"] = "serif"
    plt.rcParams["mathtext.fontset"] = "dejavuserif"
    
    if suptitle:
        suptitle = 'True class {} - Pred class {} with proba {:.4f}'.format(y, class_idx, y_hat[class_idx])
        if identifier:
            suptitle += '\n{}'.format(identifier)
        plt.suptitle(suptitle, fontsize=20, y=1.04)

        
    # Organize the plot into a grid
    grid = plt.GridSpec(x.shape[0], len(imgs), wspace=0.07, hspace=0.07)
    locs = {0: (0, slice(0,1)), 
            1: (0, slice(1,2)),
            2: (0, slice(2,3)),
            3: (1, slice(0,1)), 
            4: (1, slice(1,2)), 
            5: (1, slice(2,3)),
            6: (2, slice(0,1)), 
            7: (2, slice(1,2)),
            8: (2, slice(2,3)),
           }
     
    
    import matplotlib.patches as mpatches
    
    for i, img in enumerate(imgs):
        for j in range(img.shape[0]):
            idx = j*img.shape[0]+i
            plt.subplot(grid[locs[idx][0], locs[idx][1]])
            plt.tick_params(bottom=False, labelbottom=False, left=False, labelleft=False)
            
            colrange = np.max([abs(np.min(img[j])), abs(np.max(img[j]))])
            vmin, vmax = -colrange, colrange

            if j == 0:
                sup = r'$^+$' if posit_occ else ''
                plt.title(['Input', 'Occ ({})'.format(r'$\times$'.join(map(str, win_shape[1:]))), r'Input $\odot$ Occ' + sup][i], fontsize=20)

            if j == img.shape[0] - 1:
               plt.tick_params(bottom=True, labelbottom=True, left=False, labelleft=False)
               plt.xticks(np.linspace(0, img.shape[2]-1, 5).astype(np.int), labels=[0, 0.25, 0.5, 0.75, 1.0])
               plt.xlabel('Time [s]', fontsize=16, labelpad=10)            

            if i == 0: # Original sample
                vmin, vmax = None, None
                cmap = 'magma_r'
                plt.ylabel(['Output A', 'Output B', 'Output C'][j], fontsize=20, labelpad=10)

            elif i == 1: # Occlusion map 
                cmap = shiftedColorMap(bwr_r, name='shrunk')
                red_patch = mpatches.Patch(color='red', label=r'max: {:.3g}%'.format(np.min(img[j])*100))
                blue_patch = mpatches.Patch(color='blue', label=r'max: {:.3g}%'.format(np.max(img[j]*100)))
                plt.legend(handles=[blue_patch, red_patch],loc='upper left')
 
            elif i == 2: # Original sample * Occlusion map
                plt.yticks(np.linspace(0, img.shape[1]-1, 4).astype(np.int))
                ax = plt.gca()
                ax.yaxis.set_label_position("right")
                if j == 1:
                    plt.ylabel('Frequency channels', fontsize=16, labelpad=10)
                plt.tick_params(labelright=False, right=True)
                cmap = shiftedColorMap(bwr_r, name='shrunk')
                if posit_occ:
                    cmap = 'magma_r' 
                    vmin, vmax = None, None
            
            plt.imshow(img[j], aspect='auto', origin='lower', cmap=cmap, 
                       interpolation="none", vmin=vmin, vmax=vmax)
    
    if save_dir:   
        try:
            os.makedirs(save_dir)
        except OSError as e:
            pass
        plt.savefig(os.path.join(save_dir, identifier + '_{}{}'.format(y, ftype)),
                    dpi=dpi, bbox_inches='tight', quality=quality)
    
    if show:
        plt.show()
    plt.close()
