# Improving Machine Hearing on Limited Data Sets

## Demonstration of the experiments for reproducibility

Article: "Improving Machine Hearing on Limited Data Sets" <br> 
Authors P. Harar, R. Bammer, A. Breger, M. Dörfler and Z. Smekal<br> 
ArXiv: http://arxiv.org/abs/1903.08950

## What is in this repository

This repository contains the source code for experiment reproducibility and experiment results that were published. 
In order to reproduce the results presented in the aforementioned paper, you can preprocess the data and train the networks by opening the jupyter notebook and following the instructions. Occlusion visualization can be obtained by using the code in occlusion.ipynb.

## Installation
If you want to train the networks, ideally you should have a GPU and SSD available.
Code was developed and tested only on OS Ubuntu 18.04.

Installation within virtualenv:

* ```git clone git@gitlab.com:hararticles/gs-ms-mt.git```
* ```cd gs-ms-mt```
* ```virtualenv .venv```
* ```source .venv/bin/activate```
* ```pip install -r requirements.txt``` 
* ```jupyter notebook```
* follow the ```.ipynb``` notebook 

Installation with conda:

* ```conda create --name venv python=3.6 tensorflow-gpu=1.12.0 keras=2.2.4 pip```
* ```source activate venv```
* open the ```requirements.txt``` and remove lines containing tensorflow-gpu and keras
* ```pip install -r requirements.txt```
* ```jupyter notebook```
* follow the ```.ipynb``` notebook 

## License
This project is licensed under the terms of the MIT license. See license.txt for details.

## Acknowledgement
This work was supported by International Mobility of Researchers (CZ.02.2.69/0.0/0.0/16027/0008371) and by project LO1401. The infrastructure of the SIX Center was used.

![opvvv](/uploads/0b69ae3fea27786c5949ca7404620b33/opvvv.jpg)
